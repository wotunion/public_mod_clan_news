﻿################################################################################################
#wotunion [Mod Clan News] v.2
################################################################################################

import BigWorld
from gui import SystemMessages
from Account import Account
from json import load
from urllib2 import urlopen
from urllib2 import urlparse
import datetime
import sys    
import os
import hashlib
from shutil import copyfile
from shutil import rmtree
from PlayerEvents import g_playerEvents

InfoShowed = False
AllowOnlyAfterRestartClient = False
InfoNoticeShowed = ''
InfoNoticeGeneralShowed = ''
CurrentClanID = ''
Config = ''

################################################################################################
#Настройки перед компиляцией
################################################################################################
ConfigURL = 'http://exampleserver123qwerty.com/mod/modconfig.json'
apiURL = 'http://api.worldoftanks.ru/wot/account/info/?application_id=demo&account_id={0}'
mod_Version = 2
AllowedClanID='000000000000000000000'
################################################################################################
################################################################################################

def new_onAccountShowGUI(ctx):
    global InfoShowed 
    global InfoNoticeShowed
    global InfoNoticeGeneralShowed
    global CurrentClanID
    if  InfoShowed != True:
        player = BigWorld.player().databaseID  
        obj = load(urlopen(apiURL.format(player))) 
        CurrentClanID = obj['data'][str(player)]['clan_id']
        if  str(CurrentClanID) != AllowedClanID:  
            SystemMessages.pushMessage('<font color="#FF0000">\nКлановые оповещения не могут быть показаны. Данный мод клановых оповещений работает только для игроков клана [Клан-тег]. Пожалуйста, удалите этот мод (\res_mods\(версия клиента\scripts\client\gui\mods\mod_Clan_Info_v.(версия).pyc))</font>', type=SystemMessages.SM_TYPE.Error) #json
        else:            
            LoadConfig()
            print Config['mod']['name'] + ' v.' + str(mod_Version)
            if  Updater() == True:
                print Config['mod']['name'] + ': Updated successfully'                              
                SystemMessages.pushMessage(urlopen(Config['msg']['ModUpdatedInfoURL']).read(), type=SystemMessages.SM_TYPE.GameGreeting)                 
            else:
                SystemMessages.pushMessage('[Клан-тег] Мод Clan News\n\nМод <font color="#66ff00">активный</font>\n\n', type=SystemMessages.SM_TYPE.GameGreeting)                                    
        InfoShowed = True        
    if  str(CurrentClanID) == AllowedClanID and AllowOnlyAfterRestartClient == False:           
        if CheckActive(Config['msg']['Notice']['DaysActiveURL']) == True and Config['msg']['Notice']['Allowed'] == True:
            InfoNotice=urlopen(Config['msg']['Notice']['InfoNoticeURL']).read()
            if  InfoNoticeShowed != InfoNotice: 
                if  InfoNotice != '':
                    SystemMessages.pushMessage(InfoNotice, type=SystemMessages.SM_TYPE.GameGreeting)
                    InfoNoticeShowed = InfoNotice
        if CheckActive(Config['msg']['NoticeGeneral']['DaysActiveGeneralURL']) == True and Config['msg']['NoticeGeneral']['Allowed'] == True:        
            InfoNoticeGeneral=urlopen(Config['msg']['NoticeGeneral']['InfoNoticeGeneralURL']).read()                
            if  InfoNoticeGeneralShowed != InfoNoticeGeneral:
                if  InfoNoticeGeneral != '':    
                    SystemMessages.pushMessage(InfoNoticeGeneral, type=SystemMessages.SM_TYPE.Warning)
                    InfoNoticeGeneralShowed = InfoNoticeGeneral   
   
def new_NotificationsActionsHandlers_handleAction(self, model, typeID, entityID, actionName):
    import sys, re
    regex = re.compile(
        r'^https?://'
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|'
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'
        r'(?::\d+)?'
        r'(?:/?|[/?]\S+)$', re.IGNORECASE
    )
    if regex.match(actionName) is None:
        return old_NotificationsActionsHandlers_handleAction(self, model, typeID, entityID, actionName)
    if 'xfw' not in sys.modules:
        from gui.shared import g_eventBus, events
        g_eventBus.handleEvent(events.OpenLinkEvent(events.OpenLinkEvent.SPECIFIED, actionName))
    return None

g_playerEvents.onAccountShowGUI += new_onAccountShowGUI

from notification.actions_handlers import NotificationsActionsHandlers
old_NotificationsActionsHandlers_handleAction = NotificationsActionsHandlers.handleAction
NotificationsActionsHandlers.handleAction = new_NotificationsActionsHandlers_handleAction           

def GetModFilePath():    
    ModFileName = os.path.basename(__file__)
    RelativeModFilePath = os.path.abspath(__file__)
    WOT_Path = '\\'.join(RelativeModFilePath.split('\\')[:-5])
    return WOT_Path + '\\res_mods\\' + Config['common']['for_WOT_Version'] + Config['common']['CommonModFolder']
      
def Updater():
    global AllowOnlyAfterRestartClient
    if os.path.exists(GetModFilePath() + 'mod_Clan_News_tmp\\'):
        rmtree(GetModFilePath() + 'mod_Clan_News_tmp\\')    
    if  Config['update']['AutoUpdateAllowed'] == True and mod_Version < Config['mod']['version']:        
        NewVersionURL = Config['update']['mod_UpdateURL']
        UpdateModFilename = urlparse.urlsplit(NewVersionURL).path.split('/')[-1]
        CurrentModFileName = os.path.basename(__file__)
        CurrentModFilePath = GetModFilePath() + os.path.basename(__file__)
        ModsPath = GetModFilePath()
        TempPath = ModsPath + 'mod_Clan_News_tmp\\'
        TempFilePath = TempPath + UpdateModFilename
        os.makedirs(TempPath)
        ModFileUpdate = urlopen(NewVersionURL)
        with    open(TempFilePath,'wb') as output:
                output.write(ModFileUpdate.read())     
        if  hashlib.sha256(open(TempFilePath, 'rb').read()).hexdigest() == Config['update']['sha-256']:
            copyfile(TempFilePath, ModsPath + UpdateModFilename)
            if  CurrentModFileName != UpdateModFilename:
                os.remove(CurrentModFilePath)
                rmtree(TempPath)
            else:
                rmtree(TempPath)
            if  Config['update']['AllowOnlyAfterRestartClient'] == True:
                AllowOnlyAfterRestartClient = True                        
            return True
        else:
            print Config['mod']['name'] + ': update downloaded, but hash check failed'
            rmtree(TempPath)
            return False        
    else: 
        return False
        
def CheckActive(NoticeTypeURL):
    days_active = urlopen(NoticeTypeURL).read()    
    timenow = datetime.datetime.strftime(datetime.datetime.now(), '%d.%m.%Y %H:%M:%S')    
    if  timenow > days_active:
        return False
    else:
        return True
        
def LoadConfig():  
    global Config
    Config = load(urlopen(ConfigURL))                